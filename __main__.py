from Base import Base
from Logic import flow_logs_reader, my_reader
from Stream import stream
   
def main():
    # simple=False        # Master switch to change between flowlogs_reader library and my_reader program
    base = Base()       # Instantiate Base class to read config.yaml and do other things
    if base.SIMPLE:
        stream(HOST=base.ip_address,
                      PORT=base.port,
                      BUFFERSIZE=base.buffer_size,
                      UDP=base.socket_udp,
                      data=flow_logs_reader(base))
    elif not base.SIMPLE:
        stream(HOST=base.ip_address,
                      PORT=base.port,
                      BUFFERSIZE=base.buffer_size,
                      UDP=base.socket_udp,
                      data=my_reader(base))

if __name__=='__main__':
    main()