# docker build -f ./Dockerfile -t flowlogs-translator .
docker stop atera
docker rm atera
docker run -dit \
    --name atera \
    --network host \
    --hostname atera \
    --mount type=bind,source="$(pwd)",destination=/flowtranslator \
    --mount type=bind,source="$HOME/.aws/",destination=/root/.aws \
    flowlogs-translator
docker attach atera