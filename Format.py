from datetime import datetime

def calculate_stats(key,payload):
    s=dict()
    s = {'key':None,
         'total': 0,
         'highest_version_found':0,
         'stats': {'NODATA':0,
                   'SKIPDATA':0,
                   'OK':{'ACCEPT':0,
                         'REJECT':0}
                   }
         }
    s['key']=key
    for d in payload:
        s['total'] +=1
        if d['version'] is not None:
            if d['version'] > s['highest_version_found']:
                s['highest_version_found']=d['version']
        if d['log-status'] is not None:
            if d['action']=="NODATA": s['stats']['NODATA'] += 1
            if d['action']=="SKIPDATA": s['stats']['SKIPDATA'] += 1
            
        if d['action'] is not None:
            if d['action']=="ACCEPT": s['stats']['OK']['ACCEPT'] += 1
            if d['action']=="REJECT": s['stats']['OK']['REJECT'] += 1
    return (s)
        
def format_to_proprietary_json(all_contents):
    """
    Returns: all_payload as class<list> of json dictionaries and stats
    Takes:  A list of all selectively read flow log contents, where each list item is a dictionary with key and content values
            all_contents[0]['key'] == Flow_logs_key_identifier
            all_contents[0]['content'] == Expects class<str> contents of full un_gzipped log file with header as a part of the dictionary.
    
    Function: Reads log files in AWS v2 flow log format to re-format in proprietary json.
    
    Flow Log format (v2):
    version account-id interface-id srcaddr dstaddr srcport dstport protocol packets bytes start end action log-status
    """
    all_payload = list()    # List of all payloads with all_payload[0]['key']=.log file name; all_payload[0]['payload']=payload
    all_stats = list() # Access stats of flow-logs as sum of all files. 
    
    for c in all_contents:
        # Look at each key+content individually
        payload = list()
        lines = c['content'].splitlines()

        for i, line in enumerate(lines):
            d=dict()
            # print (i,line)
            if i==0:
                # Get Headers out from file. Not doing dynamic assignment
                # Good place to print out the header line, in case of VPC Flow Logs Format change
                # headers=line.split()
                pass
            else:
                # else, for all the value rows, create a data d dictionary, which is appended to payload
                # split into order indexed list of each value row. Assuming v2 log format with len(rows)=14 individual fields.
                rows=line.split()
                # print (rows, len(rows))

                for index, value in enumerate(rows):
                    # print (index,type(value))
                    if index==0: d['version'] = int(value)                           
                    if index==1: d['account-id'] = int(value)
                    if index==2: d['interface-id'] = str(value) if value != "-" else None
                    if index==3: d['srcaddr'] = str(value) if value != "-" else None
                    if index==4: d['dstaddr'] = str(value) if value != "-" else None
                    if index==5: d['srcport'] = int(value) if value != "-" else None
                    if index==6: d['dstport'] = int(value) if value != "-" else None
                    if index==7: d['protocol'] = int(value) if value != "-" else None
                    if index==8: d['packets'] = int(value) if value != "-" else None
                    if index==9: d['bytes'] = int(value) if value != "-" else None
                    if index==10: d['start'] = datetime.fromtimestamp(int(value)) if value != "-" else None
                    if index==11: d['end'] = datetime.fromtimestamp(int(value)) if value != "-" else None
                    if index==12: d['action'] = str(value) if value != "-" else None
                    if index==13: d['log-status'] = str(value) if value != "-" else None
                # calculate_stats(d) # Instead, calculating stats right here.
                # each log line as dict:d being appended to payload
                payload.append(d)       # add to payload list, each log line as a json dictionary
                
        # 'Each line' and 'all lines' for loop is completed. Passing 1 complete log file as payload to calculate stats
        stat = calculate_stats(c['key'],payload)
        all_payload_dict={'key':c['key'],
                          'payload':payload}
        all_payload.append(all_payload_dict)
        all_stats.append(stat)
    
    # print ("DEBUG:\n{}".format(all_stats))
    # print(payload)
    
    return (all_payload, all_stats)



def main():
    """ For local execution and testing. Disregard """
    # f1 = '/Users/adhawale/Workspace/Projects/vpc-translator/downloads/vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/20/671678195002_vpcflowlogs_us-west-1_fl-0b9e8e2ae85204ec2_20220220T0400Z_aaabd27e.log'
    # f2 = '/Users/adhawale/Workspace/Projects/vpc-translator/downloads/vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/20/671678195002_vpcflowlogs_us-west-1_fl-0b9e8e2ae85204ec2_20220220T0330Z_74c27380.log'

    # with open (f1,encoding='utf-8') as f:
    #     content = f.read()
    #     f.close()
    # all_contents = list()
    # c={'key':"my_key",
    #    'content':content
    #    }
    # all_contents.append(c)
    # a,s = format_to_proprietary_json(all_contents)
    # print (a,s)
    pass
    
if __name__=="__main__":
    main()