import json
import socket

def stream(HOST,PORT,BUFFERSIZE,UDP,data):
    # HOST = '127.0.0.1'  # The server's hostname or IP address
    # PORT = 65431        # The port used by the server
    # BUFFERSIZE=4096
    # UDP=True
    
    if not UDP:
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect((HOST, PORT))
            # s.sendall(b'Hello, world')
            # data_size=len(json.dumps(data,sort_keys=True,default=str).encode('utf-8'))
            # print(data_size)
            for d in data:
                s.sendall(json.dumps(d, sort_keys=True, default=str).encode('utf-8'))
            # data = s.recv(data_size)
            data = data.decode('utf-8')

        print('Received', repr(data))
    elif UDP:
        udpsocket=socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
        for d in data:
            udpsocket.sendto(json.dumps(d,sort_keys=True,default=str).encode('utf-8'),(HOST,PORT))
        
        reply_return = udpsocket.recvfrom(BUFFERSIZE)
        # Printing only last message:
        returned_message = reply_return[0]
        print ("DEBUG:\t{}".format(returned_message))
 
def main():
    pass

if __name__=="__main__":
    main()