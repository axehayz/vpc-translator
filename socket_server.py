import socket

HOST = '127.0.0.1'  # Standard loopback interface address (localhost)
PORT = 65431        # Port to listen on (non-privileged ports are > 1023)
BUFFERSIZE=4096
UDP=True

def main():
    if not UDP:
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.bind((HOST, PORT))
            s.listen()
            # s.recvfrom() # for DGRAM
            conn, addr = s.accept()
            with conn:
                print('Connected by', addr)
                while True:
                    data = conn.recv(BUFFERSIZE)
                    if not data:
                        break
                    conn.sendall(data)
    elif UDP:
        udpsocket=socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
        udpsocket.bind((HOST,PORT))
        while(True):
            incomming = udpsocket.recvfrom(BUFFERSIZE)
            message = incomming[0]
            ip_addr = incomming[1]
            
            # print("Client IP Address:{}".format(ip_addr))
            print("Content Length: {}".format(len(message)))
            print("\nMessage from Client:\n {}".format(message))
            # Reply back
            udpsocket.sendto(message,ip_addr)


if __name__=='__main__':
    main()