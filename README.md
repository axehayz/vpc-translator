# VPC Flow Logs Reader and Translator

---

## Instructions

A fully packaged docker container is provided to run. The socket server file is also included as a part of the same docker installation. Once the container is up and running, attach yourself to it and run the python socket_server.py in background and python module in the foreground.

Instructions provided below:

Clone the repository

```bash
git clone https://axehayz@bitbucket.org/axehayz/vpc-translator.git
cd vpc-translator
```

Build docker container locally and run `docker_run.sh` script to run docker

```bash
docker build -f ./Dockerfile -t flowlogs-translator .
```

```bash
chmod +x ./docker_run.sh
./docker_run.sh
```

Once inside the `atera` container with `bash` prompt, run the below command.

```bash
bash5.1# python socket_server.py > server.out 2>&1 &
```

This allows running a socket server in the same container as a background process. It outputs the received UDP/TCP messages to `stdout` which redirected to `server.out` file

Finally, run the python module from inside the container

```bash
bash5.1# python .
```

---

## Config

The configuration is controlled by a single `config.yaml` file. This file can be modified with options before running the docker container (via `run_docker.sh` command.)

Alternatively, the entire working path is mounted inside the docker container, and you can also modify the file from within the container before running the python module

Lets look at parts of the `config.yaml`

```yaml
AWS:
  config:
    region_name: null
    profile_name: null
  credentials:
    aws_access_key_id: null
    aws_secret_access_key: null
```

> This is optional. Keep values = `null` if not planning on using a custom AWS config / credentials. Optionally, enter the AWS config and credentials details here to use that to build session with Boto3 AWS Python SDK.
By default (and preferred), we will be mounting the `~/.aws` path inside the docker container. Boto3 sequentially tries several ways to gather credentials. Looking inside `~/.aws` is the first on the list.

```yaml
bucket:
    name: "acorp-us-west-1"
    # name: "akshay-ingest-bucket-flow-logs"
    custom_directory: "vpc-flowlogs"
    region: "us-west-1"
    easy_direct_full_prefix: null
    # easy_direct_full_prefix: "vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/13///"
  accounts:
    - 671678195002
    - 987572852212
```

> Here, you will enter all the bucket details. There are 2 options
>
> 1. Use `easy_direct_full_prefix` if you know exactly the bucket prefix to be accessed to read logs. This is the simplest form.
> If this is not available, then set value to `null` and proceed to step 2 below
> 2. Use `name` to identify the bucket name. Permission should exist to READ the bucket from accessing host
> 3. Use `custom_directory` to specify a custom prefix inside the bucket where AWS flow logs are published
> 4. Use `region` to specify region name where the bucket resides
> 5. Optional: Can specify list of accounts only for which the logs will be fetched from the given s3 bucket. Used as in-production, customers write multiple account logs into a single bucket for cost and management

```yaml
SOCKET:
  udp: True
  ip_address: 127.0.0.1
  port: 65431
  buffer_size: 4096
```

> This config specification is for socket programing.
> Enter details appropriately.
> `udp: True` is by default as its easier to packet capture and look inside. However, UDP is fairly unreliable way of communicating over the Internet to an API end point. Specifically, packets can be lost, fragmented, and may cause buffer overloads.
> This is why a `udp: False` flag is provided to stream logs over TCP instead.

```yaml
DEBUG: True
OPTIONS:
  save_gzip: True
  simple: False
```

> Other options for program.
> `DEBUG` allows verbose logging
> `save_gzip` saves the fetched log.gzip files for future access
> `simple` switch flag requires some explaination (see below)

---

## simple: True or False

There exists `simple` switch flag in `config.yaml`. This flag dictates which library to use for reading and processing AWS VPC Flow Logs before they are sent over the socket.

This flag exists because Bo, dropped a public library first week of Feb 2022, which allows reading VPC flow logs in multiple formats and produces a json output. With such an (awesome) library - the code is mere <50 lines.

Considering this as a coding exercise, I started to build my own library. This was challenging but also educating. There are many challenges one has to overcome to access, read, and format logs in _realtime_ and ready them to stream to an arbitrary endpoint as JSON encoded data in _realtime_. I will describe a few of them below:

1. Assuming, flow logs are generated and written to a given s3 bucket. Can use a custom prefix within bucket.
2. AWS indexes flow logs by filesize and time of day. This renders very hierarchical storage of the said flow logs.
   1. Note: Although it seems hierarchical, __everything__ inside a bucket is just an object(s) and the hierarchy is abstracted away as the "key".
3. The _key_ in selecting correct files (objects) is in identifying the correct `key` for the object. The `Base` class provides these methods
4. Once the right `key` is identified, then the objects are retreived and only relevant logs are selected for formatting. By default, this is the last 1 hour of the current day. This can be abstracted by providing `start` and `end` times via `config.yaml` (not currently exposed)
5. For socket programming, MacOS has some limitations on message size restrictions. Hence, implementing in Docker to avoid system dependencies.

> By default: `simple: False` invokes my library.

---

## Example run

```bash
my-machine$ ./docker_run.sh
atera
atera
4e93ea995f447bbc628b97c458f30de46b4cc89c4989abae08e79ff1295cabb2

# Inside docker container now.
# Run python socket server to listen
bash-5.1# python socket_server.py > server.out 2>&1 &
[1] 8

# Run python module
bash-5.1# python .
DEBUG:  s3.Bucket(name='acorp-us-west-1')
DEBUG:  Custom 'easy_direct_full_prefix' not found in config.yaml
DEBUG:  [caution] reading entire bucket may take time and incurr charges
INFO:   Total objects read: 30185
DEBUG:  Keys: ['vpc-flowlogs', 'vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1'] 2
INFO:   Total unique prefixes for selection: 1
DEBUG:  Key(s): ['vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1']
INFO:   Full prefix built as: 'vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/21/'
DEBUG:  Total Downloaded: 131 Total Selected: 24
DEBUG:  Attempting to save files in /flowtranslator/downloads/vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/21/
DEBUG: vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/21/671678195002_vpcflowlogs_us-west-1_fl-0b9e8e2ae85204ec2_20220221T0425Z_479933ff.log.gz (293)
DEBUG: vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/21/671678195002_vpcflowlogs_us-west-1_fl-0b9e8e2ae85204ec2_20220221T0430Z_61f709b2.log.gz (832)
DEBUG: vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/21/671678195002_vpcflowlogs_us-west-1_fl-0b9e8e2ae85204ec2_20220221T0430Z_f56a5820.log.gz (316)
DEBUG: vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/21/671678195002_vpcflowlogs_us-west-1_fl-0b9e8e2ae85204ec2_20220221T0435Z_5de9aac1.log.gz (814)
DEBUG: vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/21/671678195002_vpcflowlogs_us-west-1_fl-0b9e8e2ae85204ec2_20220221T0435Z_fe784766.log.gz (291)
DEBUG: vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/21/671678195002_vpcflowlogs_us-west-1_fl-0b9e8e2ae85204ec2_20220221T0440Z_41018d36.log.gz (286)
DEBUG: vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/21/671678195002_vpcflowlogs_us-west-1_fl-0b9e8e2ae85204ec2_20220221T0440Z_b13c7fed.log.gz (859)
DEBUG: vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/21/671678195002_vpcflowlogs_us-west-1_fl-0b9e8e2ae85204ec2_20220221T0445Z_1c1f035b.log.gz (304)
DEBUG: vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/21/671678195002_vpcflowlogs_us-west-1_fl-0b9e8e2ae85204ec2_20220221T0445Z_988dd2bf.log.gz (829)
DEBUG: vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/21/671678195002_vpcflowlogs_us-west-1_fl-0b9e8e2ae85204ec2_20220221T0450Z_4e3ae4cc.log.gz (826)
DEBUG: vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/21/671678195002_vpcflowlogs_us-west-1_fl-0b9e8e2ae85204ec2_20220221T0450Z_8637d0ac.log.gz (321)
DEBUG: vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/21/671678195002_vpcflowlogs_us-west-1_fl-0b9e8e2ae85204ec2_20220221T0455Z_24f333eb.log.gz (835)
DEBUG: vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/21/671678195002_vpcflowlogs_us-west-1_fl-0b9e8e2ae85204ec2_20220221T0455Z_779e5633.log.gz (303)
DEBUG: vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/21/671678195002_vpcflowlogs_us-west-1_fl-0b9e8e2ae85204ec2_20220221T0500Z_64ed361b.log.gz (308)
DEBUG: vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/21/671678195002_vpcflowlogs_us-west-1_fl-0b9e8e2ae85204ec2_20220221T0500Z_a0c49944.log.gz (840)
DEBUG: vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/21/671678195002_vpcflowlogs_us-west-1_fl-0b9e8e2ae85204ec2_20220221T0505Z_7209fa6c.log.gz (288)
DEBUG: vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/21/671678195002_vpcflowlogs_us-west-1_fl-0b9e8e2ae85204ec2_20220221T0505Z_fc076dfc.log.gz (839)
DEBUG: vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/21/671678195002_vpcflowlogs_us-west-1_fl-0b9e8e2ae85204ec2_20220221T0510Z_687dc709.log.gz (303)
DEBUG: vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/21/671678195002_vpcflowlogs_us-west-1_fl-0b9e8e2ae85204ec2_20220221T0510Z_f7698891.log.gz (847)
DEBUG: vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/21/671678195002_vpcflowlogs_us-west-1_fl-0b9e8e2ae85204ec2_20220221T0515Z_dcfce910.log.gz (287)
DEBUG: vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/21/671678195002_vpcflowlogs_us-west-1_fl-0b9e8e2ae85204ec2_20220221T0515Z_de14e17f.log.gz (868)
DEBUG: vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/21/671678195002_vpcflowlogs_us-west-1_fl-0b9e8e2ae85204ec2_20220221T0520Z_885cf1a4.log.gz (828)
DEBUG: vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/21/671678195002_vpcflowlogs_us-west-1_fl-0b9e8e2ae85204ec2_20220221T0520Z_d1d17b28.log.gz (303)
DEBUG: vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/21/671678195002_vpcflowlogs_us-west-1_fl-0b9e8e2ae85204ec2_20220221T0525Z_17b7d2b3.log.gz (903)
DEBUG:
[{'key': 'vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/21/671678195002_vpcflowlogs_us-west-1_fl-0b9e8e2ae85204ec2_20220221T0425Z_479933ff.log.gz', 'total': 293, 'highest_version_found': 2, 'stats': {'NODATA': 0, 'SKIPDATA': 0, 'OK': {'ACCEPT': 253, 'REJECT': 34}}}, {'key': 'vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/21/671678195002_vpcflowlogs_us-west-1_fl-0b9e8e2ae85204ec2_20220221T0430Z_61f709b2.log.gz', 'total': 832, 'highest_version_found': 2, 'stats': {'NODATA': 0, 'SKIPDATA': 0, 'OK': {'ACCEPT': 730, 'REJECT': 89}}}, {'key': 'vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/21/671678195002_vpcflowlogs_us-west-1_fl-0b9e8e2ae85204ec2_20220221T0430Z_f56a5820.log.gz', 'total': 316, 'highest_version_found': 2, 'stats': {'NODATA': 0, 'SKIPDATA': 0, 'OK': {'ACCEPT': 279, 'REJECT': 32}}}, {'key': 'vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/21/671678195002_vpcflowlogs_us-west-1_fl-0b9e8e2ae85204ec2_20220221T0435Z_5de9aac1.log.gz', 'total': 814, 'highest_version_found': 2, 'stats': {'NODATA': 0, 'SKIPDATA': 0, 'OK': {'ACCEPT': 733, 'REJECT': 71}}}, {'key': 'vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/21/671678195002_vpcflowlogs_us-west-1_fl-0b9e8e2ae85204ec2_20220221T0435Z_fe784766.log.gz', 'total': 291, 'highest_version_found': 2, 'stats': {'NODATA': 0, 'SKIPDATA': 0, 'OK': {'ACCEPT': 248, 'REJECT': 39}}}, {'key': 'vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/21/671678195002_vpcflowlogs_us-west-1_fl-0b9e8e2ae85204ec2_20220221T0440Z_41018d36.log.gz', 'total': 286, 'highest_version_found': 2, 'stats': {'NODATA': 0, 'SKIPDATA': 0, 'OK': {'ACCEPT': 250, 'REJECT': 35}}}, {'key': 'vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/21/671678195002_vpcflowlogs_us-west-1_fl-0b9e8e2ae85204ec2_20220221T0440Z_b13c7fed.log.gz', 'total': 859, 'highest_version_found': 2, 'stats': {'NODATA': 0, 'SKIPDATA': 0, 'OK': {'ACCEPT': 744, 'REJECT': 106}}}, {'key': 'vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/21/671678195002_vpcflowlogs_us-west-1_fl-0b9e8e2ae85204ec2_20220221T0445Z_1c1f035b.log.gz', 'total': 304, 'highest_version_found': 2, 'stats': {'NODATA': 0, 'SKIPDATA': 0, 'OK': {'ACCEPT': 264, 'REJECT': 36}}}, {'key': 'vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/21/671678195002_vpcflowlogs_us-west-1_fl-0b9e8e2ae85204ec2_20220221T0445Z_988dd2bf.log.gz', 'total': 829, 'highest_version_found': 2, 'stats': {'NODATA': 0, 'SKIPDATA': 0, 'OK': {'ACCEPT': 736, 'REJECT': 82}}}, {'key': 'vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/21/671678195002_vpcflowlogs_us-west-1_fl-0b9e8e2ae85204ec2_20220221T0450Z_4e3ae4cc.log.gz', 'total': 826, 'highest_version_found': 2, 'stats': {'NODATA': 0, 'SKIPDATA': 0, 'OK': {'ACCEPT': 732, 'REJECT': 86}}}, {'key': 'vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/21/671678195002_vpcflowlogs_us-west-1_fl-0b9e8e2ae85204ec2_20220221T0450Z_8637d0ac.log.gz', 'total': 321, 'highest_version_found': 2, 'stats': {'NODATA': 0, 'SKIPDATA': 0, 'OK': {'ACCEPT': 277, 'REJECT': 39}}}, {'key': 'vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/21/671678195002_vpcflowlogs_us-west-1_fl-0b9e8e2ae85204ec2_20220221T0455Z_24f333eb.log.gz', 'total': 835, 'highest_version_found': 2, 'stats': {'NODATA': 0, 'SKIPDATA': 0, 'OK': {'ACCEPT': 723, 'REJECT': 104}}}, {'key': 'vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/21/671678195002_vpcflowlogs_us-west-1_fl-0b9e8e2ae85204ec2_20220221T0455Z_779e5633.log.gz', 'total': 303, 'highest_version_found': 2, 'stats': {'NODATA': 0, 'SKIPDATA': 0, 'OK': {'ACCEPT': 266, 'REJECT': 34}}}, {'key': 'vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/21/671678195002_vpcflowlogs_us-west-1_fl-0b9e8e2ae85204ec2_20220221T0500Z_64ed361b.log.gz', 'total': 308, 'highest_version_found': 2, 'stats': {'NODATA': 0, 'SKIPDATA': 0, 'OK': {'ACCEPT': 277, 'REJECT': 30}}}, {'key': 'vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/21/671678195002_vpcflowlogs_us-west-1_fl-0b9e8e2ae85204ec2_20220221T0500Z_a0c49944.log.gz', 'total': 840, 'highest_version_found': 2, 'stats': {'NODATA': 0, 'SKIPDATA': 0, 'OK': {'ACCEPT': 737, 'REJECT': 94}}}, {'key': 'vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/21/671678195002_vpcflowlogs_us-west-1_fl-0b9e8e2ae85204ec2_20220221T0505Z_7209fa6c.log.gz', 'total': 288, 'highest_version_found': 2, 'stats': {'NODATA': 0, 'SKIPDATA': 0, 'OK': {'ACCEPT': 255, 'REJECT': 30}}}, {'key': 'vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/21/671678195002_vpcflowlogs_us-west-1_fl-0b9e8e2ae85204ec2_20220221T0505Z_fc076dfc.log.gz', 'total': 839, 'highest_version_found': 2, 'stats': {'NODATA': 0, 'SKIPDATA': 0, 'OK': {'ACCEPT': 741, 'REJECT': 88}}}, {'key': 'vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/21/671678195002_vpcflowlogs_us-west-1_fl-0b9e8e2ae85204ec2_20220221T0510Z_687dc709.log.gz', 'total': 303, 'highest_version_found': 2, 'stats': {'NODATA': 0, 'SKIPDATA': 0, 'OK': {'ACCEPT': 274, 'REJECT': 25}}}, {'key': 'vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/21/671678195002_vpcflowlogs_us-west-1_fl-0b9e8e2ae85204ec2_20220221T0510Z_f7698891.log.gz', 'total': 847, 'highest_version_found': 2, 'stats': {'NODATA': 0, 'SKIPDATA': 0, 'OK': {'ACCEPT': 745, 'REJECT': 93}}}, {'key': 'vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/21/671678195002_vpcflowlogs_us-west-1_fl-0b9e8e2ae85204ec2_20220221T0515Z_dcfce910.log.gz', 'total': 287, 'highest_version_found': 2, 'stats': {'NODATA': 0, 'SKIPDATA': 0, 'OK': {'ACCEPT': 246, 'REJECT': 40}}}, {'key': 'vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/21/671678195002_vpcflowlogs_us-west-1_fl-0b9e8e2ae85204ec2_20220221T0515Z_de14e17f.log.gz', 'total': 868, 'highest_version_found': 2, 'stats': {'NODATA': 0, 'SKIPDATA': 0, 'OK': {'ACCEPT': 751, 'REJECT': 104}}}, {'key': 'vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/21/671678195002_vpcflowlogs_us-west-1_fl-0b9e8e2ae85204ec2_20220221T0520Z_885cf1a4.log.gz', 'total': 828, 'highest_version_found': 2, 'stats': {'NODATA': 0, 'SKIPDATA': 0, 'OK': {'ACCEPT': 730, 'REJECT': 85}}}, {'key': 'vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/21/671678195002_vpcflowlogs_us-west-1_fl-0b9e8e2ae85204ec2_20220221T0520Z_d1d17b28.log.gz', 'total': 303, 'highest_version_found': 2, 'stats': {'NODATA': 0, 'SKIPDATA': 0, 'OK': {'ACCEPT': 274, 'REJECT': 26}}}, {'key': 'vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/21/671678195002_vpcflowlogs_us-west-1_fl-0b9e8e2ae85204ec2_20220221T0525Z_17b7d2b3.log.gz', 'total': 903, 'highest_version_found': 2, 'stats': {'NODATA': 0, 'SKIPDATA': 0, 'OK': {'ACCEPT': 794, 'REJECT': 98}}}]
DEBUG:  b'{"account-id": 671678195002, "action": null, "bytes": null, "dstaddr": null, "dstport": null, "end": "2022-02-21 04:28:20", "interface-id": "eni-0c2e15abdf8cb6ae9", "log-status": "NODATA", "packets": null, "protocol": null, "srcaddr": null, "srcport": null, "start": "2022-02-21 04:27:22", "version": 2}'
bash-5.1#
```

Check out the `server.out` for confirmation of packets sent (and response received)

```bash
bash-5.1# head server.out 
Content Length: 303

Message from Client:
 b'{"account-id": 671678195002, "action": null, "bytes": null, "dstaddr": null, "dstport": null, "end": "2022-02-21 04:28:20", "interface-id": "eni-0c2e15abdf8cb6ae9", "log-status": "NODATA", "packets": null, "protocol": null, "srcaddr": null, "srcport": null, "start": "2022-02-21 04:27:22", "version": 2}'
Content Length: 305

Message from Client:
 b'{"account-id": 671678195002, "action": null, "bytes": null, "dstaddr": null, "dstport": null, "end": "2022-02-21 04:28:20", "interface-id": "eni-0c2e15abdf8cb6ae9", "log-status": "SKIPDATA", "packets": null, "protocol": null, "srcaddr": null, "srcport": null, "start": "2022-02-21 04:27:22", "version": 2}'
Content Length: 317

bash-5.1# 
```
