FROM python:alpine

# Adding requirements file for python setup and installing requirements
COPY ./requirements.txt /flowtranslator/config/

# Adding bash for Alpine doesn't carry it.
# RUN apk add --no-cache bash bash-completion

RUN apk add --no-cache --virtual .build-deps gcc musl-dev && \
    apk add bash bash-completion && \
    pip install --upgrade pip && \
    pip install cython && \
    pip install --trusted-host pypi.python.org -r /flowtranslator/config/requirements.txt && \
    apk del .build-deps gcc musl-dev

# Change workdir to the application for relative path names inside container
WORKDIR /flowtranslator

COPY *.py /flowtranslator/
COPY *.yaml /flowtranslator/

# Run setup.py using pip to setup kkonsole script environment
RUN pip install --editable .

# CMD [ "python", "socket_server.py > server.out 2>&1 &"]
ENTRYPOINT ["/bin/bash"]