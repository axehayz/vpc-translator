import boto3
from datetime import datetime, timedelta
import gzip
from pathlib import Path
import os
import yaml

# st = 'vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/13/671678195002_vpcflowlogs_us-west-1_fl-0b9e8e2ae85204ec2_20220213T2345Z_1146680f.log.gz'
class Base():
    def __init__(self):
        """
        Base class to keep track of configuration and other items related to project
        """
        # Load complete yaml
        self.config = collect_config(self)
        
        # Load some individual configuration items for easy variable reference
        self.bucket_name = None or self.config['AWS']['bucket']['name']
        self.custom_dir = None or self.config['AWS']['bucket']['custom_directory']
        self.easy_direct_full_prefix = None or self.config['AWS']['bucket']['easy_direct_full_prefix']
        self.bucket_region = None or self.config['AWS']['bucket']['region']
        self.accounts_interested = None or self.config['AWS']['accounts']
        self.socket_udp = None or self.config['SOCKET']['udp']
        self.ip_address = None or self.config['SOCKET']['ip_address']
        self.port = None or self.config['SOCKET']['port']
        self.buffer_size = None or self.config['SOCKET']['buffer_size']
        self.DEBUG = None or self.config['DEBUG']
        self.SAVE = None or self.config['OPTIONS']['save_gzip']
        self.SIMPLE = None or self.config['OPTIONS']['simple']
        # Other required parameters
        self.session = None     # Initializing to None for 1/0 checks. Requires _build_session()
        self.full_prefix = None
        self.now = datetime.utcnow()
        self.selected_files = list()    # List(S3BucketObject=vpc-flow-gzip-log-file)
        self.s3 = None

              
    def _build_session(self):
        """
        Ideally, should pick up your default preference of session build for boto3 client.
        For Docker, you can mount your ~/.aws path inside the docker container for default access.
        If all that fails, can explicitly use the config.yaml to define a custom session to be used
        """
        
        s3_session = boto3.session.Session(aws_access_key_id=self.config['AWS']['credentials']['aws_access_key_id'], 
                                           aws_secret_access_key=self.config['AWS']['credentials']['aws_secret_access_key'],  
                                           region_name=self.config['AWS']['config']['region_name'], 
                                           profile_name=self.config['AWS']['config']['profile_name'])
        self.session = s3_session
        
    def _build_prefix(self, bucket):
        """
        AWS Flowlogs have a prefix hierarchy of the format:
        custom_prefix/AWSLogs/account_id/vpcflowlogs/region/YYYY/MM/DD/filehash.log.gz
        "vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/13/671678195002_vpcflowlogs_us-west-1_fl-0b9e8e2ae85204ec2_20220213T2345Z_1146680f.log.gz"
        Note: Bucket Name is not considered a part of prefix.
        
        In production, have encountered various ways of implementing flow log collection in S3 bucket. 
        One per region. One per account. All in one bucket. LCM to delete logs that are older. 
        To not overwhelm the ingest pipeline with years' worth of records, it is must that we read the correct prefix.
        This internal method helps with the prefix build to select only relevent objects (flow logs)
        
        Alternatively, it is easiest to provide a full prefix.
        """
        if self.easy_direct_full_prefix:
            self.easy_direct_full_prefix = self.easy_direct_full_prefix.rstrip("/")+"/"
            print ("Using fully qualified prefix provided: '{}'".format(self.easy_direct_full_prefix))
            # vpc-flowlogs/AWSLogs/671678195002/vpcflowlogs/us-west-1/2022/02/13
        else:
            if self.DEBUG:
                print ("DEBUG:\tCustom 'easy_direct_full_prefix' not found in config.yaml")
                print ("DEBUG:\t[caution] reading entire bucket may take time and incurr charges")
            objects = list(bucket.objects.all())
            print ("INFO:\tTotal objects read: {}".format(len(objects)))
            # Retreive Keys for to analyze unique prefix
            keys = list()
            for object in objects:
                selected_prefix = object.key.rsplit("/",4)[0]  # Removing the hash, day, month, year. To be added later.
                if selected_prefix not in keys:
                    keys.append(selected_prefix)
            if self.DEBUG: print ("DEBUG:\tKeys: {0} {1}".format(keys, len(keys)))
            if self.custom_dir is not None:
                if self.custom_dir in keys:
                    keys.remove(self.custom_dir)
            print ("INFO:\tTotal unique prefixes for selection: {0}".format(len(keys)))
            if self.DEBUG: print ("DEBUG:\tKey(s): {0}".format(keys))
            # Select latest prefix to be built further
            prefix = keys[-1].rstrip("/")+"/"
            # print(prefix)
            
            # Assuming there are current logs for today in realtime. 
            # Not building inputting start/end times but can be built using config.yaml
            
            date_prefix = (self.now - timedelta(hours=1)).strftime('%Y/%m/%d/')
            full_prefix=prefix+date_prefix
            self.full_prefix = full_prefix
            print ("INFO:\tFull prefix built as: '{0}'".format(full_prefix))                
        
    def _collect_s3_logs(self, bucket):
        """
        pure python boto3 (Amazon Python SDK) based implementation to access flow logs
        Returns: None
        """
        s3_resource = boto3.resource('s3')
        
        # Adjusting prefixes for final time
        if self.easy_direct_full_prefix is not None:
            self.full_prefix = self.easy_direct_full_prefix
        
        # Select necessary file objects
        if self.DEBUG: i=0
        for file in bucket.objects.filter(Prefix= self.full_prefix):
            if self.DEBUG: i+=1
            # Important step to only include results from start time to end time. By default: Last 1 hour. 
            if (self.now.replace(tzinfo = None) > file.last_modified.replace(tzinfo = None) > (self.now-timedelta(hours=1)).replace(tzinfo = None)):
                self.selected_files.append(file)
        if self.DEBUG:
            print ("DEBUG:\tTotal Downloaded: {0} Total Selected: {1}".format(i,len(self.selected_files))) # Prints total downloaded files, selected files from start/end time
            # print (self.selected_files)
        
    def _read_s3_logs(self, bucket):
        """
        Returns: A list of all responses from selected files , where each response is a dictionary with key and content
                 The response['key'] is S3.Object.Key for identification of flow logs. 
                 And response['content'] is the log for given_key.log (<str>) 
                 
        Input:  Method of the class Base. Expects the S3.Bucket object construct as argument.
        
        Function:   Reads gzip files from selected prefixes and returns read contents. 
                    Format: (v2)
                    version account-id interface-id srcaddr dstaddr srcport dstport protocol packets bytes start end action log-status
        
        Since this method decompresses a gzip log file and reads the 'contents' 
        It is prudent to also do the saving and format processing, 
        while the contents are looked at to avoid re-reading and allocating more memory or reading from disk
        """
        if self.SAVE:
            # If SAVE=True, create the directory to save log files.
            path = os.getcwd()+"/downloads/"+self.full_prefix
            Path(path).mkdir(parents=True, exist_ok=True)
            if self.DEBUG: print("DEBUG:\tAttempting to save files in {0}".format(path))
        else:
            pass

        # For each selected s3.Bucket.FileObject (Gzip Log files as S3 objects)
        all_read_contents_list = []
        for k in self.selected_files:
            response={'key': None, 'content': None}
            response['key']=k.key
            if self.s3 is not None:
                obj = self.s3.Object(bucket_name=k.bucket_name, key=k.key)
                with gzip.GzipFile(fileobj=obj.get()["Body"]) as gzipfile:
                    # Read the file
                    # response['content'] = content
                    response['content'] = gzipfile.read().decode('utf-8')
                    
                    if self.DEBUG: 
                        # DEBUG. Prints key and #Log-Lines each key (file)
                        print ("DEBUG: {0} ({1})".format(response['key'], len(response['content'].splitlines())-1))  
                    if self.SAVE:
                        # Save while file is open in memory
                        file_handle = path+k.key.rsplit('/',1)[1]
                        # if self.DEBUG: print ("DEBUG:\tAttempting to save to {0}".format(file_handle))
                        with gzip.open(file_handle, 'w') as f:
                            f.write(response['content'].encode('utf-8'))
                    
                    all_read_contents_list.append(response)
        return(all_read_contents_list)
  
def collect_config(self):
    with open("config.yaml", "r") as config_file:
        try:
            config = yaml.safe_load(config_file)
            return config
        except yaml.YAMLError as exc:
            print(exc)
    config_file.close()


def main():
    pass
    
if __name__=="__main__":
    main()