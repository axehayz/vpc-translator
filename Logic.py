from flowlogs_reader import FlowLogsReader, S3FlowLogsReader
import Format

def flow_logs_reader(base):
    payload_to_send = list()
    
    # reader = S3FlowLogsReader('akshay-ingest-bucket-flow-logs/vpc-0051d97a4ac17683b', include_regions=['us-west-1'])
    reader = S3FlowLogsReader(base.bucket_name+"/"+base.custom_dir  , include_regions=['us-west-1'])
    records = list(reader)
    
    if base.DEBUG: print ("DEBUG:\t{}".format(len(records)))
    
    for o in records:
        payload_to_send.append(o.to_dict())
    # print(len(payload_to_send))
    # print(payload_to_send[0])
    return (payload_to_send)


def my_reader(base):
    # base = Base()
    base._build_session()
    s3 = base.session.resource('s3')    # Creating a High-level S3 resource which has in-built pagination (as opposed to a low-level 'Client' utility)
    base.s3 = s3                        # Storing the session
    bucket = s3.Bucket(base.bucket_name)# Accessing actual bucket in python object 'bucket'
    
    if base.DEBUG: print ("DEBUG:\t{}".format(bucket))  # Debug

    base._build_prefix(bucket)          # Build prefix that needs to be supplied to S3.Boto3 client/resource for correct object selection
    base._collect_s3_logs(bucket)       # Collect all necessary fileobjects and select based on given time (default = 1 hour)
    ac = base._read_s3_logs(bucket)     # Read logs from retreived .gzip fileobjects
    
    # Now pass to formatter to output required json format and optionally also return statistics on processed logs
    all_contents, all_stats = Format.format_to_proprietary_json(ac)
    
    if base.DEBUG: print ("DEBUG:\n{}".format(all_stats))   # Debug
    
    payload_to_send=list()
    for c in all_contents:
        # print (type(c))
        for p in c['payload']:
            payload_to_send.append(p)
    return (payload_to_send)
    # Stream.stream(1,2,3,0,payload_to_send)
    # send_logs()
    
def main():
    pass

if __name__=='__main__':
    main()